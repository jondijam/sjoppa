<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::filter('admin', function(){
    if (!Auth::user()->hasRole(1))
    {
        return Redirect::to('admin/login');
    }
});

Route::get('/admin', 'AdminController@dashboard');
Route::get('/admin/login', 'AdminController@login');
Route::post('/admin/login', 'AdminController@loggingIn');
Route::get('/admin/categories', 'AdminController@categories');
Route::get('/admin/categories/edit/{id}','AdminController@editCategory');
Route::post('/admin/categories/save/{id}', 'AdminController@saveCategory');
Route::post('/admin/categories/delete/{id}', 'AdminController@deleteCategory');
Route::get('/admin/logout', 'AdminController@logout');
Route::get('/admin/products', 'AdminController@products');

Route::get('/admin/orders', 'AdminController@orders');
Route::get('/admin/order/view/{id}', 'AdminController@viewOrder');
Route::post('/admin/order/delete/{id}', 'AdminController@deleteOrder');

Route::get('/admin/products/edit/{id}', 'AdminController@editProduct');
Route::post('/admin/products/save/{id}', 'AdminController@saveProduct');

Route::get('/admin/attributes', 'AdminController@attributes');
Route::get('/admin/suppliers', 'AdminController@suppliers');

Route::post('/admin/products/images/primary', 'AdminController@editPrimaryImages');
Route::post('/admin/products/image/delete/{id}', 'AdminController@deleteImage');
Route::post('/admin/products/images/viewImage', 'AdminController@viewImage');
Route::post('/admin/products/upload', 'AdminController@upload');
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/users/edit/{id}', 'AdminController@editUser');
Route::post('/admin/users/save/{id}', 'AdminController@saveUser');


Route::get('/', function(){
	echo getenv('DB_USERNAME');
});



Route::get('/cart', 'OrderController@cart');
Route::post('/cart/update/{id}', 'OrderController@updateCart');
Route::post('/cart/delete/{id}', 'OrderController@deleteCart');
Route::get('/order/information', 'OrderController@information');
Route::post('/order/shipping', 'OrderController@shipping');
Route::post('/home/addToCart', 'HomeController@addToCart');

Route::get('/account/login', 'AccountController@login');
Route::post('/account/login', 'AccountController@loggingIn');
Route::get('/account/orders', 'AccountController@orders');
Route::get('/account/profile', 'AccountController@profile');
Route::post('/account/profile/save/{id}', 'AccountController@saveProfile');
Route::get('/account/register', 'AccountController@register');
Route::post('/account/register', 'AccountController@saveUser');
Route::post('/order/error', 'OrderController@error');
Route::post('/order/server', 'OrderController@server');
Route::get('/order/cancel', 'OrderController@cancel');
Route::post('/order/summary', 'OrderController@summary');
Route::post('/order/update/{id}', 'OrderController@update');

Route::get('/{args}', 'HomeController@category')->where('args', '(.*)');



