<?php namespace Mail\Shipping;
use SoapClient;
 
class Shipping 
{
	public static function soapClient()
	{
		$client = new \SoapClient("https://api.postur.is/sap/bc/srt/wsdl/bndg_E10C782F9C42E4F199F3001A64362F30/wsdl11/allinone/standard/document?sap-client=020", array('login' => "INT_PUBLIC", 'password' => "AllurPakkinn123"));
		return $client;
	}

 	public static function get_shipping_cost($item, $customerPostcode, $senderPostcode, $customerCountry, $orderFragile)
 	{
 		$client = self::soapClient();

 		$shippingCost = $client->ZWsGetpriceAllMailTypes(array(
			"InAdviceOfDelivery"=>"",
			"InCashOnDelivery"=>"", 
			"InContractOnDelivery" => "", 
			"InDeliverToAddresseOnly"=>"",
			"InDeliveryType"=>"H", 
			"InDestinationCountryCode"=>$customerCountry, 
			"InDestinationPostalCode"=>$customerPostcode, 
			"InExpressDelivery"=>"", 
			"InFragile"=>$orderFragile, 
			"InInsurance"=>"", 
			"InInsuranceValue"=>"2000", 
			"InInsuranceValueCurrency"=>"ISK", 
			"InLarge"=>"", 
			"InLengthUnit"=>"CM", 
			"InMailClass"=>"P", 
			"InSenderDoesCustomDeclarat"=>"", 
			"InSentFromPostOffice"=>$senderPostcode, 
			"InWeightUnit"=>"KG", 
			"InlistItems"=>$item));
		
		//ZWsGetCountryListResponse
		//$get_price = $client->ZWsGetpriceAllMailTypes(0,0,0,0,0,0,0);
		//$client->__soapCall("SomeFunction", array($a, $b, $c));

		//return $client->ZWsGetPostCodeList->OutlistPostCodes->item();

		return $shippingCost;
 	}

 	public static function pre_shipping_registration($senderSSN, $senderName, $senderAddress, $senderZip, $senderEmail, $senderPhone, $shippingName, $shippingAddress, $shippingZip, $shippingCity, $shippingCountry, $shipping_pays, $fragile, $items)
 	{
 		$client = self::soapClient();
 		$shippingRegistration = $client->ZWsPreregAllMailTypes(array(
 			'InSender' => array('CustomerId' => $senderSSN, 
 				'NameLine1' => $senderName,
 				'NameLine2' => '',
 				'AddressLine1' => $senderAddress, 
 				'PostalCode' => $senderZip, 
 				'Email' => $senderEmail, 
 				'Phone' => $senderPhone),
 			'InAddressee' => array(
 				'Kennitala' => '',
 				'NameLine1' => $shippingName, 
 				'NameLine2' => '',
 				'AddressLine1' => $shippingAddress, 
 				'PostalCode' => $shippingZip,
 				'Email' => '', 
 				'PhoneForSms' => '',
 				'City' => $shippingCity, 
 				'IsoCountryCode' => $shippingCountry),
 			'InMailOptions' => array(
 				'InstructionsForNonDelivery' => '', 
 				'AddressePaysFee' => $shipping_pays, 
 				'AdviceOfDelivery' => 'x', 
 				'MailClass'=>'P', 
 				'DeliveryType'=>'H',
 				'Large'=>'',
 				'ExpressDelivery' => '',
 				'WeightUnit' => 'KG',
 				'DimensionUnit' => 'CM',
 				'DeliverToAddresseOnly' => '',
 				'Insurance' => '',
 				'InsuranceValue'=>'',
 				'InsuranceValueCurrency'=>'',
 				'StorageDays'=>'30',
 				'Contents'=>'',
 				'Comments'=>'',
 				'Fragile' => $fragile,
 				'ContractOnDelivery' => '',
 				'Insurance' => '',
 				'SentFromPostOffice'=>'400',
 			),
 			'InCashOnDelivery' => array(
 				'Cod' => '', 
 				'CodValue'=>'0', 
 				'CodValueCurrency' => '', 
 				'Slipnr' => '', 
 				'Reference' => '', 
 				'CodPreDefined' => '', 
 				'CodBank' => '',
 				'CodAccType' => '',
 				'CodAccount' => '',
 				'CodAccOwner' => '',

 				),
 			'InlistItems' => $items));

 		return $shippingRegistration;
 	}
 	 
  public static function greeting()
  {
    return "What up dawg";
  }
 
}