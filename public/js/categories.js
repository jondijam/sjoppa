var category_id = 0;
var c = false;

$('.delete').click(function(e){
	e.preventDefault();
	category_id = $(this).attr('data-category-id');

	c = confirm("Do you want to delete this category?");

	console.log(c);
	if (c === true) 
	{
		console.log('delete');
		$.post('/admin/categories/delete/'+category_id, {}, function(data){
			$('#'+category_id).remove();
		});
	}
});