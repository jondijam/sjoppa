var unit_price_incl_tax = 0;
var qty = 0;
var subTotal = 0;
var total = 0;
var cart_id = 0;
var results = [];
var children;
var childrenLength

$('.delete').click(function(){
  
  cart_id = $(this).attr('data-cart-id');


  $.post('/cart/delete/'+cart_id, {}, function(){
      total = 0;

      children = $(".cartBody").children();
      if (children.length == 1)
      {
         window.location = "/cart";
      }
      else
      {

        $('#cart'+cart_id).remove();
        for (var i = 0; i < children.length; i++) 
        {
          total += parseInt($('#'+children[i].id).attr("data-subTotal"));     
        }

        $('#total').text(total);
      }
  });

 

});

$('.update').click(function(e)
{
	total = 0;
	cart_id = $(this).attr('data-cart-id');
	unit_price_incl_tax = $(this).attr('data-unit-price-tax-incl');
	qty = $('.qty').val();
	subTotal = qty * unit_price_incl_tax;


    $('#subtotal'+cart_id).text(subTotal);
    $('#cart'+cart_id).attr('data-subTotal', subTotal);
    children = $(".cartBody").children();
    
    for (var i = 0; i < children.length; i++) 
    {
    	total += parseInt($('#'+children[i].id).attr("data-subTotal"));    	
   	}

   	$('#total').text(total);

   	$.post('/cart/update/'+cart_id, {cart_id:cart_id, qty:qty, subtotal:subTotal});
});