$(document).ready(function()
{
	var name = "";
	var referenceCode = "";
	var active = 0;
	var availableOrder = 0;
	var onlineOnly = 0;
	var shortDescription = "";
	var description = ""
	var wholeSalePrice = 0;
	var retailPrice = 0;
	var discount = 0.00;
	var saleTax = 0.00;
	var categories = new Array();
	var width = 0.00;
	var height = 0.00;
	var length = 0.00;
	var weight = 0.00;
	var qty = 0;
	var outOfStock = 0;
	var product_id = 0;

	$('#save').click(function(e){
		e.preventDefault();

		name = $('.name').val();
		referenceCode = $('.referenceCode').val();
		
		if ($('#active').prop('checked')) 
		{
			active = $('#active:checked').val();
		};

		if ($('.availableOrder').prop('checked')) 
		{
			availableOrder = $('.availableOrder:checked').val();
		}

		if ($('.onlineOnly').prop('checked')) 
		{
			onlineOnly = $('.onlineOnly:checked').val();
		}
		
		shortDescription = $('#shortDescription').val();
		description = $('.description').val();
		wholeSalePrice = $('#wholePrice').val();
		retailPrice = $('.retailPrice').val();
		discount = $('.discount').val();
		saleTax = $('.saleTax').val();

		$("input[name='categories[]']:checked").each(function ()
		{
    		categories.push($(this).val());
		});

		width = $('.width').val();
		height = $('.height').val();
		length = $('.length').val();
		weight = $('.weight').val();
		qty = $('.qty').val();


		if ($('#outOfStock').prop('checked')) 
		{
			outOfStock = $('#outOfStock:checked').val();
		};		
		
		product_id = $(this).attr('data-product-id');

		$.post('/admin/products/save/'+product_id, {
			name:name, 
			referenceCode:referenceCode, 
			active:active, 
			availableOrder:availableOrder, 
			onlineOnly:onlineOnly, 
			condition:"new", 
			shortDescription:shortDescription, 
			description:description, 
			wholeSalePrice:wholeSalePrice, 
			retailPrice:retailPrice, 
			saleTax:saleTax, 
			categories:categories, 
			width:width, 
			height:height, 
			length:length, 
			weight:weight, 
			qty:qty, 
			outOfStock:outOfStock, 
			discount:discount}, 
			function(data){
				if(data == 'passes')
				{
					//$('.error').addClass('hidden');
					window.location = "/admin/products";
				}
				else
				{
					$('.error').removeClass('hidden');
					$('.error').html(data)
				}
			}
		);
	});

	$('#addToCart').click(function(e){
		e.preventDefault();
		product_id = $(this).attr('data-product-id');
		qty = $('#qty').val();
		
		$.post('/home/addToCart', {productId:product_id, qty:qty}, function(){
			$('#addToCart').addClass('hidden');
			$('#addTocartSuccess').removeClass('hidden');

			setTimeout(function(){

				$('#addTocartSuccess').addClass('hidden');
				$('#addToCart').removeClass('hidden');
				
			}, 3000);
		});

	});
});