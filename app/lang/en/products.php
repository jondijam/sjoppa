<?php  
	return [
	'Products' => 'Products',
	'Attributes' => 'Attributes',
	'Orders' => '{0} :qty orders|{1} :qty order|[0, Inf] :qty orders',
	'Stocks' => '{0} Out of stock|[1,Inf] There are :qty copies left',
	]

?>