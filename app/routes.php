<?php  
	
	Route::get('/', ['as'=>'home', 'uses'=>'HomeController@index']);
	Route::resource('products', 'ProductsController', ['only' => ['index', 'show']]);
	Route::resource('profiles', 'ProfilesController');
	Route::resource('carts', 'CartsController');
	Route::resource('orders', 'OrdersController');
	Route::resource('categories', 'CategoriesController');

	Route::resource('categories.products', 'CategoryProductController', [
		'only' => ['index', 'show']
	]);

	Route::get('register', [
		'as'=>'register', 
		'uses'=>'RegistrationController@create'
	]);
	
	Route::resource('registration', 'RegistrationController', 
		['only' => ['create', 'confirm', 'confirmed', 'store']
	]);


	Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
	Route::get('logout', ['as'=>'logout', 'uses' => 'SessionsController@destroy']);
	Route::resource('sessions', 'SessionsController',  array('only' => array('create', 'store', 'destroy')));

	Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
		
		Route::group(['before' => 'role:admin'], function()
		{
			Route::get('/', ['as'=>'admin', 'uses'=>'AdminController@index']);
			Route::get('/help', ['as'=>'admin.help', 'uses'=>'AdminController@help']);

			Route::resource('users', 'UsersController', array('only'=> array('create', 'index', 'edit', 'update', 'destroy')));
			
			App::bind('Sjoppa\Products\ProductRepositoryInterface', 'Sjoppa\Products\DbProductRepository');

			Route::resource('products', 'ProductsController');
			Route::resource('attributes', 'AttributesController');
			Route::resource('orders', 'OrdersController');
			Route::resource('categories', 'CategoriesController');
			Route::get('logout', ['as'=>'admin.logout', 'uses' => 'SessionsController@destroy']);

		});

		
		Route::get('login', ['as' => 'admin.login', 'uses' => 'SessionsController@create']);
		

		Route::resource('sessions', 'SessionsController',  array('only' => array('create', 'store', 'destroy')));

		
	});

	Route::get('/{args}', 'CategoryController@v')->where('args', '(.*)');


	

?>