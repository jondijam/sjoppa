<?php
class AccountController extends BaseController
{
	public function login()
	{
		if (!Auth::check()) 
		{
			(array) $categories = array();
			$categories = Category::where('parent', 0)->get();
			$data['categories'] = $categories;
			return View::make('account/login', $data);
		}
		else
		{
			return Redirect::to('account/to');
		}
	}

	public function loggingIn()
	{	
		$username = "";
		$password = "";

		$username = Input::get('username');
		$password = Input::get('password');
		
    	if ( Auth::attempt(array('username' => $username, 'password' => $password, 'confirmed'=>true))) 
    	{
    		return Redirect::intended('account/profile');
    	}
    		
    	return Redirect::back()->withInput()->withErrors('That username/password combo does not exist.');			
	}

	public function register()
	{
		if (Auth::check())
		{
			return Redirect::to('account/profile');
		}
		else
		{
			(array) $categories = array();
			
			$categories = Category::where('parent', 0)->get();
			$data['categories'] = $categories;

			return View::make('account/register', $data);
		}
	}

	public function saveUser()
	{
		$username = "";
		$password = "";
		$email = "";

		$rules = array(
			'username' => array('required'),
			'password' => array('required'),
			'email' => array('required'),
			'confirmedPassword'=> array('same:password')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes()) 
		{
			$username = Input::get('username');
			$email = Input::get('email');
			$password = Input::get('password');
			$password = Hash::make($password);

			$user = new User();

			$user->email = $email;
			$user->username = $username;
			$user->password = $password;
			$user->role = 0;
			$user->confirmed = 1;
			$user->confirmation = 0;
			$user->save();

			return Redirect::to('account/login');
		}
		else
		{
			return Redirect::to('account/register')->withErrors($validator);
		}

	}

	public function logout()
	{
		if (Auth::check()) 
		{
			Auth::logout();
			return Redirect::to('account/login');
		}
		else
		{
			return Redirect::to('account/login');
		}
	}

	public function profile()
	{
		(array) $data = array();
		(int) $customerId = 0;
		(string) $name = "";
		(string) $company = "";
		(string) $phone = "";
		(string) $email = "";
		(string) $address = "";
		(string) $address2 = "";
		(string) $zip = "";
		(string) $city = "";
		(string) $country = "";
		
		if (Auth::check()) 
		{
			$categories = Category::where('parent', 0)->get();
			$data['categories'] = $categories;
			
			$user = Auth::user();
			if($user->customer()->count() > 0)
			{
				$customerId = $user->customer->id;
				$name = $user->customer->name;
				$email = $user->customer->email;
				$phone = $user->customer->phone;
				$address = $user->customer->address;
				$address2 = $user->customer->address2;
				$zip = $user->customer->zip;
				$city = $user->customer->city;
				$country = $user->customer->country;
				$company = $user->customer->company;
			}

			

			$data['customerId'] = $customerId; 
			$data['name'] = $name;
			$data['email'] = $email;
			$data['phone'] = $phone;
			$data['address'] = $address;
			$data['address2'] = $address2;
			$data['zip'] = $zip;
			$data['city'] = $city;
			$data['country'] = $country;
			$data['company'] = $company;
 			
			return View::make('account/profile', $data);
		}
		else
		{	
			return Redirect::to('account/login');
		}
	}

	public function saveProfile($customerId)
	{
		if(Auth::check())
		{
			
			(array) $customer = array();
			(array) $user = array();
			(string) $name = "";
			(string) $company = "";
			(string) $phone = "";
			(string) $email = "";
			(string) $address = "";
			(string) $address2 = "";
			(string) $zip = "";
			(string) $city = "";
			(string) $country = "";

			$user = Auth::user();
			$name = Input::get('name');
			$company = Input::get('company');
			$phone = Input::get('phone');
			$email = Input::get('email');
			$address = Input::get('address');
			$address2 = Input::get('address2');
			$zip = Input::get('zip');
			$city = Input::get('city');
			$country = Input::get('country');

			$rules = array(
				'name' => array('required'),
				'phone' => array('required'),
				'email' => array('required', 'email'),
				'address' => array('required'),
				'zip' => array('required'),
				'city' => array('required'),
				'country' => array('required')
			);	

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->passes()) 
			{
				$sessionId = Session::getId();

				if ($customerId > 0) 
				{
					$customer = Customer::find($customerId);
				}
				else
				{
					$customer = new Customer;
				}

					$customer->name = $name;
					$customer->session_id = $sessionId;
					$customer->email = $email;
					$customer->phone = $phone;
					$customer->address = $address;
					$customer->address2 = $address2;
					$customer->zip = $zip;
					$customer->city = $city;
					$customer->country = $country;
					$customer->company = $company;
					$customer->user()->associate($user);
					$customer->save();

			}
			else
			{

			}
		}
	}

	public function orders()
	{
		if (Auth::check()) 
		{
			(array) $categories = array();
			(array) $orders = array();
			(array) $user = array();

			$user = Auth::user();
			$customer = $user->customer->first();
			$orders = $customer->orders()->paginate(10);

			$categories = Category::where('parent', 0)->get();
			$data['categories'] = $categories;
			$data['orders'] = $orders;


			return View::make('account/orders', $data);
		}
		else
		{
			return Redirect::to('account/login');
		}
	}

	public function viewOrder($orderId)
	{
		(array) $data = array();
		(array) $orderEntry = array();
		(array) $products = array();

 		(double) $totalPaid = 0;
		(double) $totalShipping = 0;
		(double) $totalTax = 0;

		$orderEntry = Order::find($orderId);


		$shippingDate = $orderEntry->shipping_date;
		$carrier = $orderEntry->carrier;
		$weight = $orderEntry->weight;
		$trackingNumber = $orderEntry->tracking_number;
		$totalShipping = $orderEntry->total_shipping;
		$totalPaid = $orderEntry->total_paid;
		
		$data['carrier'] = $carrier;
		$data['shippingDate'] = $shippingDate;

		return View::make('account/order/view', $data);
	}
}
?>