<?php 
	namespace Admin;
	
	use View;
	use Auth;
	use Sjoppa\Repo\Product\ProductInterface;
	use Sjoppa\Repo\Order\OrderInterface;
	use Sjoppa\Repo\User\UserInterface;

	class AdminController extends \BaseController 
	{
		protected $product;
		protected $order;
		protected $user;

		public function __construct(ProductInterface $product, OrderInterface $order)
		{
			$this->product = $product;
			$this->order = $order;
		}

		public function index()
		{
			$title = 'Admin - dashboard';
			$getLatestOrders = $this->order->getLatest(); 
			$popularProducts = $this->product->getByOrders();

			return View::make('admin.index', compact('title', 'getLatestOrders', 'popularProducts'));	
		}
		
		public function help()
		{

		}
	}
?>