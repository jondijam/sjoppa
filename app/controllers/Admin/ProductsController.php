<?php
namespace Admin;

use Sjoppa\Repo\Product\ProductInterface;
use View;
use Input;
use Paginator;

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /products
	 *
	 * @return Response
	 */
	protected $product;

	public function __construct(ProductInterface $product)
	{
		$this->product = $product;
	}
	
	public function index()
	{

		$products = array();
		$title = 'Admin - Products';
		$page = Input::get('page', 0);
 
 	 	$pagiData = $this->product->getByPage($page, 10);

 	 	dd($pagiData);

 	 	$products = Paginator::make($pagiData->items, $pagiData->totalItems, $pagiData->perPage);

		return View::make('admin.products.index', compact('products', 'title'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /products/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /products
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /products/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = $this->product->getById($id);

		return View::make('admin.products.edit', compact('product'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}