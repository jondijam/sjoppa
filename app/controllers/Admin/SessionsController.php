<?php
namespace Admin;

use View;
use Auth;
use Input;
use Redirect;
use Session;
use Sjoppa\Forms\LoginForms;
use Lang;

//use Input;

//use Redirect;

class SessionsController extends \BaseController 
{
	protected $LoginForms;

	public function __construct(LoginForms $LoginForms)
	{
		$this->LoginForms = $LoginForms;
		$this->beforeFilter('csrf',  ['only' => ['store']]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /sessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$title = 'sjoppa.is - Admin Login';	
		$error = Session::get('message');
		$errorMessage = "";

		
		return View::make('admin.sessions.create', compact('title', 'error'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function store()
	{
	
		$input = Input::only('username', 'password');
		$validateErrors = array();

		if($this->LoginForms->validate($input))
		{
			if (Auth::attempt($input)) 
			{
				return Redirect::route('admin')->with('message', 'Welcome');
			}	
		}

		$validateErrors = $this->LoginForms->getValidationErrors();
			
		return Redirect::back()->withErrors($validateErrors, 'loginForm')->with('message', Lang::get('Sessions.LoginFailed'));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{

	}

}