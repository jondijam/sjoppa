<?php 
class OrderController extends BaseController 
{
	public function cart()
	{
		(array) $carts = array();
		(array) $data = array();
		(array) $userEntry = array();
		(string) $sessionId = "";
		(int) $userId = 0;

		$sessionId = Session::getId();

		if (Auth::check()) 
		{
			$userEntry = Auth::user();
			$userId = $userEntry->user_id;

			$carts = Cart::where('user_id', $userId)->get();
		}
		else
		{
			$carts = Cart::where('session_id', $sessionId)->get();
		}

		$categories = Category::where('parent', 0)->get();
		$data['categories'] = $categories;
		$data['carts'] = $carts;

		return View::make('home/cart', $data);
	}

	public function deleteCart($cart_id)
	{
		(array) $cartEntry = array();
		$cartEntry = Cart::find($cart_id);
		$cartEntry->delete();
	}

	public function updateCart($cart_id)
	{
		(array) $cartEntry = array();

		$subtotal = Input::get('subtotal');
		$qty = Input::get('qty');

		$cartEntry = Cart::find($cart_id);
		$cartEntry->total = $subtotal;
		$cartEntry->qty = $qty;
		$cartEntry->save(); 
	}

	public function information()
	{
		(array) $categories = array();
		(array) $data = array();

		$customerName = "";
		$customerEmail = "";
		$customerPhone = "";
		$customerAddress = "";
		$customerAddress2 = "";
		$customerCompany = "";
		$customerZip = "";
		$customerCity = "";
		$customerCountry = "";
		$sessionId = "";
		$customerId = 0;

		$categories = Category::where('parent', 0)->get();
		
		if (Auth::check()) 
		{
			$user = Auth::user();
			if (count($user->customer) > 0) 
			{
				$customerId = $user->customer->id;
				$customerName = $user->customer->name;
				$customerEmail = $user->customer->email;
				$customerAddress = $user->customer->address;
				$customerAddress2 = $user->customer->address2;
				$customerCompany = $user->customer->company;
				$customerPhone = $user->customer->phone;
				$customerZip = $user->customer->zip;
				$customerCity = $user->customer->city;
				$customerCountry = $user->customer->country;	# code...
			}
		}
		else
		{
			$sessionId = Session::getId();
			$customer = Customer::where('SessionId', $sessionId)->first();
			if (count($customer) > 0) 
			{
				$customerId = $customer->id;
				$customerName = $customer->name;
				$customerAddress = $customer->address;
				$customerAddress2 = $customer->address2;
				$customerCompany = $customer->company;
				$customerPhone = $customer->phone;
				$customerZip = $customer->zip;
				$customerCity = $customer->city;
				$customerCountry = $customer->country;	
			}
		}


		$data['categories'] = $categories;
		$data['customerId'] = $customerId;
		$data['customerName'] = $customerName;
		$data['customerEmail'] = $customerEmail;
		$data['customerPhone'] = $customerPhone;
		$data['customerAddress'] = $customerAddress;
		$data['customerAddress2'] = $customerAddress2;
		$data['customerCompany'] = $customerCompany;
		$data['customerZip'] = $customerZip;
		$data['customerCity'] = $customerCity;
		$data['customerCountry'] = $customerCountry;

		
		return View::make('home/information', $data);
	}

	public function shipping()
	{
		(array) $data = array();
		(array) $rules = array();

		(array) $carts = array();
		(array) $products = array();
		(array) $shipping_cost = new stdClass();
		(string) $session_id = ""; 
		
		(string) $customer_name = "";
		(string) $customer_email = "";
		(string) $customer_phone = "";
		(string) $customer_company = "";
		(string) $customer_address = "";
		(string) $customer_address2 = "";
		(string) $customer_zip = "";
		(string) $customer_city = "";
		(string) $customer_country = "";
		(int) $customer_id = 0;

		(boolean) $customer_is_shipping = false;
		(boolean) $customer_subscribe = false;
		
		(string) $shipping_name = "";
		(string) $shipping_email = "";
		(string) $shipping_phone = "";
		(string) $shipping_company = "";
		(string) $shipping_address = "";
		(string) $shipping_address2 = "";
		(string) $shipping_zip = "";
		(string) $shipping_city = "";
		(string) $shipping_country = "";
		(double) $total_discounts = 0;
		(double) $total_discounts_tax_incl = 0;
		(double) $total_discounts_tax_excl = 0;
		(double) $total_paid_tax_incl = 0;
		(double) $total_paid_tax_excl = 0; 
		(double) $total_products = 0;

		(array) $items = array();
		(array) $order_customers = array();
	
		$customer_is_shipping = Input::get('customerIsShipping');
		$customer_id = Input::get('customerId');
		$customer_name = Input::get('customerName');
		$customer_email = Input::get('customerEmail');
		$customer_phone = Input::get('customerPhone');
		$customer_company = Input::get('customerCompany');
		$customer_address = Input::get('customerAddress');
		$customer_address2 = Input::get('customerAddress2');
		$customer_zip = Input::get('customerZip');
		$customer_city = Input::get('customerCity');
		$customer_country = Input::get('customerCountry');
		$customer_subscribe = Input::get('customerSubscribe');

		$shipping_name = Input::get('shippingName');
		$shipping_email = Input::get('shippingEmail');
		$shipping_phone = Input::get('shippingPhone');
		$shipping_company = Input::get('shippingCompany');
		$shipping_address = Input::get('shippingAddress');
		$shipping_address2 = Input::get('shippingAddress2');
		$shipping_zip = Input::get('shippingZip');
		$shipping_country = Input::get('shippingCountry');
		
		$session_id = Session::getId();
		$sender_zip = 400;
		$fragile = "";

			if ($customer_is_shipping) 
		{
			$rules = array(
				'customerName' => 'required',
				'customerEmail' => array('required', 'email'),
				'customerAddress' => array('required'),
				'customerCity' => array('required'),
				'customerZip' => array('required'),
			);
		}
		else
		{
			$rules = array('customerName' => 'required',
				'customerEmail' => array('required', 'email'),
				'customerAddress' => array('required'),
				'customerCity' => array('required'),
				'customerZip' => array('required'),
				'shippingName' => 'required',
				'shippingEmail' => array('required', 'email'),
				'shippingAddress' => array('required'),
				'shippingCity' => array('required'),
				'shippingZip' => array('required'),
			);
		}

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) 
		{
			if ($customer_id == 0)
			{
				$customer = New Customer();
			}
			else
			{
				$customer = Customer::find($customer_id);
			}

				$customer->session_id = $session_id;
				$customer->name = $customer_name;
				$customer->email = $customer_email;
				$customer->phone = $customer_phone;
				$customer->address = $customer_address;
				$customer->address2 = $customer_address2;
				$customer->zip = $customer_zip;
				$customer->city = $customer_city;
				$customer->country = $customer_country;
				$customer->company = $customer_company;

			if (Auth::check()) 
			{
				$user = Auth::user();
				$user_id = $user->user_id;
				$carts = Cart::where('user_id', $user_id)->get();
				$total_carts =  Cart::where('user_id', $user_id)->count();
				$customer->user()->associate($user);
			}
			else
			{
				$carts = Cart::where('session_id', $session_id)->get();
				$total_carts = Cart::where('session_id', $session_id)->count();
			}

				$customer->save();
				$customer_id = $customer->id;

			if($total_carts > 0)
			{
				foreach ($carts as $cart)
				{
					for ($i=0; $i < $cart->qty; $i++) 
					{
						$items['item'][$i]['Weight'] = number_format($cart->product->weight, 3, ".", ",");
						$items['item'][$i]['Length'] = number_format($cart->product->length, 0, ",", ".");
						$items['item'][$i]['Width'] =  number_format($cart->product->width, 0, ",", ".");
						$items['item'][$i]['Height'] = number_format($cart->product->height, 0, ",", ".");
					}

					$total_discounts_tax_incl += $cart->total;
					$total_discounts_tax_excl += $cart->total / ((100 + $cart->product->sale_tax) / 100);
					
					$products[$cart->product_id]['product_qty'] = $cart->qty;
					$products[$cart->product_id]['total_price_tax_incl'] = $cart->total;
					$products[$cart->product_id]['total_price_tax_excl'] = $cart->total / (($cart->product->sale_tax + 100) / 100);
			
				}

				$total_paid_tax_incl += $total_discounts_tax_incl;
				$total_paid_tax_excl += $total_discounts_tax_excl;

				if ($customer_is_shipping)
				{
					$shipping_cost = Shipping::get_shipping_cost($items, $customer_zip, $sender_zip, $customer_country, $fragile);
					$order_customers[$customer_id]['is_shipping'] = 1;
					$order_customers[$customer_id]['is_payer'] = 1;
				} 
				else
				{
					$shipping = New Customer();
					$shipping->session_id = $session_id;
					$shipping->name = $shipping_name;
					$shipping->email = $shipping_email;
					$shipping->phone = $shipping_phone;
					$shipping->address = $shipping_address;
					$shipping->address2 = $shipping_address2;
					$shipping->zip = $shipping_zip;
					$shipping->city = $shipping_city;
					$shipping->country = $shipping_country;
					$shipping->company = $shipping_company;
					$shipping->save();
					$shipping_id = $shipping->customer_id;

					$order_customers[$customer_id]['is_shipping'] = 0;
					$order_customers[$customer_id]['is_payer'] = 1;

					$order_customers[$shipping_id]['is_shipping'] = 1;
					$order_customers[$shipping_id]['is_payer'] = 0;

					$shipping_cost = Shipping::get_shipping_cost($items, $shipping_zip, $sender_postcode, $shipping_country, $fragile);

				}

				$total_paid_tax_incl += $shipping_cost->OutBruttoAmount;
				$total_paid_tax_excl += $shipping_cost->OutNettoAmount;
				$total_products = $total_paid_tax_incl - $shipping_cost->OutBruttoAmount;

				$order = new Order;

				$order->carrier = 'Íslandspóstur';
				$order->carrier_tax_rate = $shipping_cost->OutlistPriceDetails->item->VatPersent;
				$order->weight = $shipping_cost->OutVolWeight;
				$order->payment = "Kreditkort";
				$order->payment_status = "Waiting payment";
				$order->status = "in progress";

				$order->total_discounts = $total_discounts_tax_incl;
				$order->total_discounts_tax_incl = $total_discounts_tax_incl;
				$order->total_discounts_tax_excl = $total_discounts_tax_excl;
				$order->total_paid = $total_paid_tax_incl;
				$order->total_paid_tax_incl = $total_paid_tax_incl;
				$order->total_paid_tax_excl = $total_paid_tax_excl;
				$order->total_paid_real = 0;
				$order->total_products = $total_products;
				$order->total_shipping = $shipping_cost->OutBruttoAmount;
				$order->total_shipping_tax_incl = $shipping_cost ->OutBruttoAmount;
				$order->total_shipping_tax_excl = $shipping_cost->OutNettoAmount;
				$order->total_shipping_tax = $shipping_cost->OutVatAmount;
				$order->save();

				$order_id = $order->order_id;

				$order_entry = Order::find($order_id);
				$order_entry->products()->sync($products);
				$order_entry->customers()->sync($order_customers);
				$order_entry->save();

				$categories = Category::where('parent', 0)->get();

				$merchant_id = 9275444;
				$secure_code = 99887766;
				$paymentgatewayid = 16;

				$return_success = url('order/summary', $parameters = array(), $secure = null);
				$return_success_server = url('order/server', $parameters = array(), $secure = null); 
				$return_error = url('order/error', $parameters = array(), $secure = null);
				$return_cancel = url('order/cancel', $parameters = array(), $secure = null);
				$checkhash = md5($merchant_id.$return_success.$secure_code);

				$data['carts'] = $carts;
				$data['orderId'] = $order_id;
				$data['shippingDescription'] = $shipping_cost->OutlistPriceDetails->item->Description;
				$data['shippingTotal'] = $shipping_cost->OutBruttoAmount;
				$data['shippingQty'] =  $shipping_cost->OutlistPriceDetails->item->Quantity;
				$data['totalProducts'] = $total_products;
 				$data['merchantId'] = $merchant_id;
 				$data['total_paid_tax_incl'] = $total_paid_tax_incl;
 				$data['paymentgatewayid'] = $paymentgatewayid;
 				$data['checkhash'] = $checkhash;
				$data['categories'] = $categories;
				$data['returnSuccessServer'] = $return_success;
				$data['returnSuccess'] = $return_success;
				$data['returnError'] = $return_error;
				$data['returnCancel'] = $return_cancel; 
				$data['total_carts'] = $total_carts;

				return View::make('home/shipping', $data);
				
			}
			else
			{
				return Redirect::to('cart');
			}
		}
		else
		{
			return Redirect::to('order/information')->withErrors($validator);
		}
	}

	public function update($order_id)
	{
		(int) $carrierId = 0;
		(string) $carrier = "";
		(double) $totalShipping = 0;
		(double) $totalProducts = 0;
		(array) $orderEntry = array();

		$carrierId = Input::get('carrier');
		$totalShipping = Input::get('totalShipping');
		$totalProducts = Input::get('totalProducts');

		$orderEntry = Order::find($order_id);

		if ($carrierId == 1) 
		{
			$carrier = "Íslandspóstur";
		}
		else
		{
			$carrier = "Sótt í verslun";
		}

		$orderEntry->carrier = $carrier;
		$orderEntry->total_paid = $totalProducts + $totalShipping;
		$orderEntry->total_paid_tax_incl = $totalProducts + $totalShipping;
		$orderEntry->total_shipping = $totalShipping;
		$orderEntry->total_shipping_tax_incl = $totalShipping;
		$orderEntry->save();
	}

	public function summary()
	{
		(array) $order = array();
		(array) $products = array();
		(int) $product_qty = 0;
		(array) $categories = array();
		(string) $senderSSN = "";
		(string) $sender_name = "";
		(string) $sender_address = "";
		(string) $sender_zip = "";
		(string) $sender_email = "";
		(string) $sender_phone = "";
		(string) $fragile = "";
		(string) $shipping_name = "";
		(string) $shipping_address = "";
		(string) $shipping_zip = "";
		(string) $shipping_city = "";
		(string) $shipping_country = "";
		(string) $tracking_number = "";
		(array) $items = array();
		(array) $itemsLabel = array();
		(string) $authorizationcode = "";
		(string) $shipping_url = ""; 
		(string) $shipping_pays = "";
		
		$categories = Category::where('parent', 0)->get();
		$authorizationcode = Input::get('authorizationcode');
		$creditcardnumber = Input::get('creditcardnumber');
		$order_id = Input::get('orderid');
		$status = Input::get('status');
		$sender_SSN = "7101720489";
		$sender_name = "Smiðjan - verslun";
		$sender_address = "Sindragata 12";
		$sender_zip = "400";
		$sender_city = "Isafjordur";
		$sender_country = "IS";

		$order = Order::find($order_id);
		$shipping = $order->customers()->where('is_shipping', 1)->first();
		
		$shipping_name = $shipping->name;
		$shipping_address = $shipping->address;
		$shipping_address2 = $shipping->address2;
		$shipping_city = $shipping->city;
		$shipping_country = $shipping->country;
		$carrier = $order->carrier;
		$products = $order->products()->get();

		if ($carrier == 'Íslandspóstur') 
		{
			foreach ($products as $key => $product)
			{
				for ($i=0; $i < $product->pivot->product_qty; $i++)
				{
					$items['item'][$i]['Weight'] = number_format($product->weight, 3, ".", ",");
					$items['item'][$i]['Length'] = number_format($product->length, 0, ",", ".");
					$items['item'][$i]['Width'] =  number_format($product->width, 0, ",", ".");
					$items['item'][$i]['Height'] = number_format($product->height, 0, ",", ".");
					$items['item'][$i]['LabelNumber'] = "";
				}
			}

			$shipping_registration = Shipping::pre_shipping_registration($sender_SSN, $sender_name, $sender_address, $sender_zip, $sender_email, $sender_phone, $shipping_name, $shipping_address, $shipping_zip, $shipping_city, $shipping_country, $shipping_pays, $fragile, $items);
			
			$items = $shipping_registration->OutlistItems->item;
			$poststod = $shipping_registration->OutlistPoststod->item;
			$shipping_url = $shipping_registration->OutPrintUrl;
			$tracking_number = $shipping_registration->OutTotal->RefKey;

			foreach ($products as $key => $product) 
			{
				$product_id = $product->pivot->product_id;
				for ($i=0; $i < $product->pivot->product_qty; $i++)
				{
					$shipping_entry = new Shippingproduct;
					$shipping_entry->label_number = $items[$i]->LabelNumber;
					$shipping_entry->product()->associate($product);
					$shipping_entry->order()->associate($order);
					$shipping_entry->save();
				}

				$product_entry = Product::find($product_id);
				if (($product_entry->qty - $product->pivot->product_qty) == 0) 
				{
					$product_entry->out_of_stock = 1;
				}
				
				$product_entry->qty = ($product_entry->qty - $product->pivot->product_qty);
				$product_entry->save();
			}

			$shipping_order = new Shippingorder;

			$shipping_order->tracking_number = $tracking_number;
			$shipping_order->mail_url = $shipping_url;
			$shipping_order->order()->associate($order);
			$shipping_order->save();

			$order->payment_status = 'Paid';
			$order->status = 'Handling';
			$order->total_paid_real = $order->total_paid_tax_incl;
			$order->save();

			$payment = new Payment;
			$payment->currency = 'ISK';
			$payment->amount = $order->total_paid_tax_incl;
			$payment->payment_method = "CreditCard";
			$payment->transactiond_id = $authorizationcode;
			$payment->card_number = $creditcardnumber;
			$payment->order()->associate();
			$payment->save();

			$data['order_id'] = $order_id;
			$data['products'] = $products;
			$data['items'] = $items;
			$data['categories'] = $categories;

			return View::make('home/summary', $data);
		}
	}

	public function server()
	{
		$authorizationcode = Input::get('authorizationcode');
		$creditcardnumber = Input::get('creditcardnumber');
		$orderId = Input::get('orderid');
		$status = Input::get('status');
	}

	public function cancel()
	{
		$order_id = 0;
		$order_id = Input::get('orderid');

		$order = Order::find($order_id);

		$order->payment_status = 'Cancel';
		$order->status = 'Cancel';
		$order->total_paid_real = 0;
		$order->save();

	}

	public function error()
	{
		$errors = Input::get();
		var_dump($errors);
	}
}

?>