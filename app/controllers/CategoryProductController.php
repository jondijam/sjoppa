<?php

class CategoryProductController extends \BaseController 
{

	/**
	 * Display a listing of the resource.
	 * GET /category.products
	 *
	 * @return Response
	 */
	public function index($categoryId)
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /category.products/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($categoryId, $productId)
	{
		//
	}

}