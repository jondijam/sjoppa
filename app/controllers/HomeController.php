<?php  
use Sjoppa\Repo\Product\ProductInterface;
use Sjoppa\Repo\Order\OrderInterface;

class HomeController extends BaseController
{
	protected $product;
	protected $order;
	protected $user;

	public function __construct(ProductInterface $product, OrderInterface $order)
	{
			$this->product = $product;
			//$this->order = $order;
			//$this->user = $user;
	}

	public function index()
	{
		return View::make('index');
	}
}
?>