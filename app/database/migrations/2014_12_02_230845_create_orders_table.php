<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('session_id');
			$table->integer('user_id')->default(0);
			$table->decimal('total_paid_incl_tax', 5, 2);
			$table->decimal('total_paid_excl_tax', 5, 2);
			$table->decimal('total_handling_price_incl_tax', 5, 2)->default(0);
			$table->decimal('total_shipping_incl_tax')->default(0);
			$table->decimal('total_shipping_excl_tax')->default(0);
			$table->decimal('total_tax_ammount');
			$table->string('shipping_method');
			$table->string('payment_status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
