<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
    	{
    		$table->engine = 'InnoDB';
        	$table->increments('id');
        	$table->string('email')->unique();
        	$table->string('username')->unique();
        	$table->string('password');
        	$table->integer('role_id');
        	$table->boolean('confirmed');
        	$table->string('confirmation');
        	$table->rememberToken();
        	$table->timestamps();
    	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
