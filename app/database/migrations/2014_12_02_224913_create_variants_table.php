<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('variants', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name')->unique();
			$table->string('reference_code')->nullable();
			$table->string('ean-13')->nullable();
			$table->integer('product_id');
			$table->integer('impact_on_price');
			$table->decimal('variant_price',5,2)->default(0);
			$table->decimal('wholesale_price',5,2)->default(0);
			$table->integer('impact_on_weight')->default(0);
			$table->decimal('variant_weight',5,2)->default(0);
			$table->integer('minimum_quantity');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attribute_values');
	}

}
