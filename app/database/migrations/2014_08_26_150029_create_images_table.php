<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function($table){
			$table->increments('image_id');
        	$table->integer('product_id');
        	$table->string('src');
        	$table->boolean('primary');
        	$table->integer('height');
        	$table->integer('width');
        	$table->string('alt');
        	$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}

}
