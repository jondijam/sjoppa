<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('active');
			$table->boolean('preorder');
			$table->boolean('online_only');
			$table->text('ean13')->nullable();
			$table->text('upc')->nullable();
			$table->text('name');
			$table->text('slug');
			$table->text('reference')->nullable();
			$table->text('supplier_reference')->nullable();
			$table->text('description');
			$table->decimal('wholesale_price_incl_tax', 5, 2);
			$table->decimal('price_excl_tax', 5, 2);
			$table->decimal('discount', 5, 2);
			$table->integer('quantity')->default(0);
			$table->integer('minimal_quantity')->default(1);
			$table->boolean('in_stock');
			$table->decimal('width');
			$table->decimal('height');
			$table->decimal('length');
			$table->decimal('weight');
			$table->integer('tax_id');
			$table->integer('supplier_id');
			$table->integer('manufacturer_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
