<?php 
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vader = DB::table('users')->insert([
                'username'   => 'jondijam',
                'email'      => 'jondi@formastudios.com',
                'password'   => Hash::make('123'),
                'confirmed' => true,
                'confirmation' => 'confirmed',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
    }
 
}
?>