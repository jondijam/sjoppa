<?php

class Role extends \Eloquent 
{
	protected $table = 'roles';
	protected $primaryKey = 'id';
	protected $fillable = [];

	public function users()
	{
		return $this->belongsToMany('User')->withTimestamps();
	}

	
}