<?php

class Product extends \Eloquent {
	protected $fillable = [];
	protected $table = 'products';

	public static function boot()
	{
		static::saved(function()
		{
			Cache::forget('query.products.page');
			Cache::forget('query.products.count');
		});
	}

	public function orders()
	{
		return $this->belongsToMany('Order');
	}
}