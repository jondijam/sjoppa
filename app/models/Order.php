<?php

class Order extends \Eloquent 
{
	protected $fillable = [];
	protected $table = 'orders';

	public function products()
	{
		return $this->belongsToMany('Product');
	}

	public function profile()
	{

	}
}