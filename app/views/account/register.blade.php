<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="/account/register" method="post">
							<h2 class="text-center"><strong>Nýskráning</strong></h2>
							<div class="form-group">
								<label>Netfang</label>
								<input type="text" class="form-control" name="email" />
							</div>
							<div class="form-group">
								<label>Notendanafn</label>
								<input type="test" class="form-control" name="username" />
							</div>
							<div class="form-group">
								<label>Lykilorð</label>
								<input type="password" class="form-control" name="password" />
							</div>
							<div class="form-group">
								<label>Staðfesta lykilorð</label>
								<input type="password" class="form-control" name="confirmPassword" />
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" name="login" value="Nýskráning" þ>
							</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
</body>