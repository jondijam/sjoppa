<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<form action="/account/profile" method="post">
				<div class="panel-body">
					<div class="col-sm-6" id="login">
						<h2 class="text-center">Grunnupplýsingar</h2>
							<div class="form-group">
								<label>Nafn</label>
								<input type="text" class="form-control" name="customerName" value="{{$name}}" />
							</div>
							<div class="form-group">
								<label>Fyrirtæki</label>
								<input type="text" class="form-control" name="customerCompany" value="{{$company}}" />
							</div>
							<div class="form-group">
								<label>Netfang</label>
								<input type="text" class="form-control" name="customerEmail" value="{{$email}}" />
							</div>
							<div class="form-group">
								<label>Símanúmer</label>
								<input type="text" class="form-control" name="customerPhone" value="{{$phone}}" />
							</div>
					</div>
					<div class="col-sm-6">
						<h2 class="text-center">Heimilisfang</h2>
						<div class="form-group">
							<label>Heimilisfang</label>	
							<input type="text" class="form-control" name="customerAddress" value="{{$address}}" />
						</div>
						<div class="form-group">
							<label>Heimilisfang 2</label>
							<input type="text" class="form-control" name="customerAddress2" value="{{$address2}}" />
						</div>
						<div class="form-group">
							<label>Póstnúmer</label>
							<input type="text" class="form-control" name="customerZip" value="{{$zip}}" />
						</div>
						<div class="form-group">
							<label>Bær</label>
							<input type="text" class="form-control" name="customerCity" value="{{$city}}" />
						</div>
						<div class="form-group">
							<label>Land</label>
							<select name="customerCountry" class="form-control">
								<option value="IS">Ísland</option>
							</select>
						</div>
						<div class="form-group">
							<input type="submit" id="saveProfile" class="btn btn-primary" data-customer-id="{{$customerId}}" value="Vista" />
						</div>
					</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/account.js"></script>
</body>