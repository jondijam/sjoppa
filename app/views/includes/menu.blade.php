<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
	       		<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      	</button>
      		<a class="navbar-brand" href="/">Sjoppa.is Admin</a>
		</div>
	 	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	 		@if(Auth::check() && Auth::user()->hasRole('admin'))
	 		<ul class="nav navbar-nav">
	 			<li>{{ link_to_route('admin', Lang::get('Admin.Dashboard')) }}</li>
	 			<li>{{ link_to_route('admin.products.index', Lang::get('Products.Products'))}}</li>
	 			<li>{{ link_to_route('admin.attributes.index', Lang::get('Products.Attributes'))}}</li>
	 			<li>{{ link_to_route('admin.orders.index', Lang::get('Products.Orders'))}}</li>
	 		</ul>
	 	 	<ul class="nav navbar-nav navbar-right">
	 	 		<li>{{ link_to_route('admin.help', Lang::get('Admin.Help')) }}</li>
	 	 		<li>{{ link_to_route('admin.users.edit', Lang::get('Users.Account'), $parameters = array(Auth::user()->username), $attributes = array())}}</li>
	 	 		<li>{{ link_to_route('admin.logout', Lang::get('Sessions.Logout'))}}</li>
	 	 	
	 	 	</ul>
	 	 	@endif
	 	</div>
	</div>
</nav>