<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li @if($slug == $category->slug) class="active" @endif>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="page-header" id="banner">
		<div class="row">
			<div class="col-lg-8 col-md-7 col-sm-6">
            <h1>{{$category_name}}</h1>
            @if(count($subCategories) > 0)
            <p class="subcat">
            	<span>Undirflokkar</span>        	
            	@foreach($subCategories as $subCategory)
            		<a href="/{{$args}}/{{$subCategory->slug}}">{{$subCategory->name}}</a>
            	@endforeach
            </p>
            @endif
          </div>
		</div>
			</div>
		<div class="row">
			@if(count($products) > 0)
				@foreach($products as $product)
					<div class="col-sm-5 col-md-3 product">
					@if(count($product->images()->where('primary', 1)->first()) > 0 )
						<a href="/{{$args}}/{{$product->slug}}">
							<img src="{{$product->images()->where('primary', 1)->first()->src}}" alt="{{$product->name}}" class="img-responsive img-thumbnail" />
						</a>
					@else
					<a href="/{{$args}}/{{$product->slug}}">
						<img src="/img/dummy.png" alt="{{$product->name}}" class="img-responsive img-thumbnail" />
					</a>

					@endif
					
					<h3 style="text-align:center">
						<a href="/{{$args}}/{{$product->slug}}"><strong>{{$product->name}}</strong></a>
					</h3>
						@if(((100 - $product->discount) / 100) < 1) 
							<div class="sale col-xs-6">
								<label class="label label-primary pull-right">Útsala</label>
							</div>
							<div class="sale col-xs-6">
								<p class="@if(((100 - $product->discount) / 100) < 1) line-through pull-left @endif">
					    			<strong>{{$product->retail_price_tax_excl * (($product->sale_tax + 100)/100)}} kr.</strong>
								</p>
							</div>
							<p class="price">
								<strong>{{ number_format(($product->retail_price_tax_excl * (($product->sale_tax + 100)/100)) * ((100 - $product->discount) / 100), 0, ',','.') }} kr.</strong>
							</p>
						@else
							<p class="price">
								<strong>{{ number_format( ($product->retail_price_tax_excl * (($product->sale_tax + 100)/100)),0, ',','.')}} kr.</strong>
							</p>
						@endif
					</div>
				@endforeach
			@endif
		</div>
	</div>
</body>