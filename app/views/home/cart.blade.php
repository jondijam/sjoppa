<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="page-header" id="banner">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-6">
					<h1>Karfa þín</h1>
				</div>
          	</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				@if(count($carts) > 0)
					<h2>{{count($carts)}} vara / vörur í körfunni.</h2>					
				@endif
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="cart">
							@if(count($carts) > 0)
								<table class="table">
									<thead>
										<tr>
											<th>Vara</th>
											<th>Fjöldi</th>
											<th>Verð</th>
											<th>Samtals</th>
											<th>Uppfæra/Eyða</th>
										</tr>
									</thead>
									<tbody class="cartBody">
										<?php $total = 0; ?>
										@foreach($carts as $cart)
										<tr class="cart" data-subTotal="{{round($cart->total)}}" id="cart{{$cart->cart_id}}">
											<td>{{$cart->product->name}}</td>
											<td>
												<select class="form-control qty" name="qty">
													@for ($i = 1; $i < 100; $i++)
													    <option value="{{$i}}" @if($i == $cart->qty) selected="selected" @endif>{{$i}}</option>
													@endfor
												</select>
											</td>
											<td>{{round($cart->unit_price_tax_incl)}}</td>
											<td>
												<span id="subtotal{{$cart->cart_id}}">{{round($cart->total)}}</span> kr.
											</td>
											<td>
												<button class="btn btn-primary update" data-unit-price-tax-incl="{{round($cart->unit_price_tax_incl)}}" data-cart-id="{{$cart->cart_id}}">Uppfæra</button>
												<button class="btn btn-danger delete" data-cart-id="{{$cart->cart_id}}">Eyða</button>
											</td>
										</tr>
										<?php
											$total += round($cart->total);
										?>
										@endforeach
									</tbody>
								</table>
								<div class="row">
									<div class="col-sm-6"></div>
									<div class="col-sm-6"><h3>Samtals <small><span id="total" data-total="{{$total}}">{{$total}}</span> kr.</small></h3></div>
								</div>
							@else
								<h2>Engar vörur í körfunni.</h2>
							@endif
						</div>
					</div>
				</div>
				@if(count($carts) > 0)
				<a href="/order/information" class="btn btn-primary">Upplýsingar</a>
				@endif
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/cart.js"></script>
</body>