<!DOCTYPE html>
<html>
	<head>
		@include('home/head')
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="/">Sjoppa.is</a>
			    </div>
			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      	<ul class="nav navbar-nav">
			      	@foreach($categories as $category)
			      		<li @if($slug == $category->slug) class="active" @endif>
			      			<a href="/{{$category->slug}}">{{$category->name}}</a>
			      		</li>
			      	@endforeach
			      	</ul>
			      	<ul class="nav navbar-nav navbar-right">
			        	<li>
			        		<a href="/cart">Karfan mín</a></li>
			        			<li class="dropdown">
			          				<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
				          	<ul class="dropdown-menu" role="menu">
				            	<li><a href="/account/profile">Prófíl</a></li>
				            	<li><a href="/account/orders">Mínar pantanir</a></li>
				         	</ul>
			        	</li>
		      		</ul>
		    	</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div class="container">
			<div class="page-header" id="banner">
				<div class="row">
					<div class="col-lg-8 col-md-7 col-sm-6">
						<h1>{{$productName}}</h1>
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-sm-8">
					@if(count($primaryImage) > 0)
					<img src="{{$primaryImage->src}}" alt="" class="img-responsive img-thumbnail">
					@endif
				</div>
				<div class="col-sm-4">
					<h2>Verð: <small>{{$price}} kr.</small></h2>
					<p>{{$description}}</p>
					<form class="form-inline" role="form">
						<div class="form-group">
							<label>Fjöldi</label>
							<select class="form-control" name="qty" id="qty">
								@for ($i = 1; $i < 100; $i++)
								    <option value="{{$i}}">{{$i}}</option>
								@endfor
							</select>
						</div>
						<div class="form-group">
							<button class="btn btn-primary" id="addToCart" data-product-id="{{$productId}}">Setja i korfu</button>
							<button class="btn btn-success hidden" id="addTocartSuccess">Sett í körfu	</button>
						</div>
					</form>
					<hr />
					<ul class="media-list">
					@foreach($images as $image)
						<li class="thumbnail col-sm-3" style="margin-right:10px">
					      <img class="media-object" src="{{$primaryImage->src}}" alt="">
						</li>
					@endforeach
					  
					</ul>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
      	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
      	<script type="text/javascript" src="/js/products.js"></script>
	</body>
</html>