<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="panel panel-default">
					<div class="panel-body">
					<h3>Pöntun númer <small>{{$orderId}}</small></h3>
						<table class="table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Vara</th>
										<th>Fjöldi</th>
										<th>Verð</th>
										<th>Samtals</th>
									</tr>
								</thead>
								<tbody>
									<?php $total = 0; ?>
									@foreach($carts as $cart)
									<tr>
										<td>{{$cart->product->ProductId}}</td>
										<td>{{$cart->product->name}}</td>
										<td>{{$cart->qty}}</td>
										<td data-unit-price-tax-incl="{{number_format($cart->unit_price_tax_incl, 0, '','')}}">{{number_format($cart->unit_price_tax_incl, 0, ',','.')}} kr.</td>
										<td data-total="{{number_format($cart->total,0,'','')}}" id="subtotal{{$cart->cart_id}}">
										{{number_format($cart->total,0,',','.')}} kr.</td>
									</tr>
									@endforeach
									<tr>
										<td>
											
										</td>
										<td>
											{{$shippingDescription}}
										</td>
										<td>
											{{number_format($shippingQty,0,'','')}} 
										</td>
										<td class="totalshipping">
											{{number_format($shippingTotal,0,',','.')}} kr.
										</td>
										<td class="totalshipping">
											{{number_format($shippingTotal,0,',','.')}} kr.
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td>Samtals</td>
										<td class="total">{{number_format($total_paid_tax_incl, 0, ',', '.')}} kr.</td>
									</tr>
								</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-2">
				<div class="radio">
						<label><input type="radio" value="1" class="carrier" name="carrier" data-order-id="{{$orderId}}" data-total-products="{{$totalProducts}}" data-total-shipping="{{$shippingTotal}}" checked="checked" />Heimsending með Íslandspósti </label>
					</div>
					<div class="radio">
						<label><input type="radio" value="2" class="carrier" name="carrier" data-order-id="{{$orderId}}" data-total-products="{{$totalProducts}}" data-total-shipping="0" />Sækja í verslun Smiðjunnar Sindragötu 12c, 400 Ísafirði</label>
					</div>
				<form action="https://test.borgun.is/SecurePay/default.aspx" method="post" />
					<div class="form-group">
						<input type="hidden" name="merchantid" value="{{$merchantId}}" />
						<input type="hidden" name="paymentgatewayid" value="{{$paymentgatewayid}}">
						<input type="hidden" name="orderid" value="{{$orderId}}" />
						<input type="hidden" name="checkhash" value="{{$checkhash}}" />
						<input type="hidden" name="amount" class="totalAmount" value="{{number_format($total_paid_tax_incl, 2, ',','')}}" /> 
						<input type="hidden" name="currency" value="ISK" />
						<input type="hidden" name="language" value="IS" />
						@foreach($carts as $key => $cart)
							<input type="hidden" name="itemdescription_{{$key}}" value="{{$cart->product->name}}" />
							<input type="hidden" name="itemcount_{{$key}}" value="{{$cart->qty}}" />
							<input type="hidden" name="itemunitamount_{{$key}}" value="{{number_format($cart->unit_price_tax_incl, 2, ',','')}}" />
							<input type="hidden" name="itemamount_{{$key}}" value="{{number_format($cart->total, 2, ',','')}}" />
						@endforeach
						<div class="shippingBox">
							<input type="hidden" class="shippingPay" name="itemdescription_{{$total_carts}}" value="{{$shippingDescription}}" />
							<input type="hidden" class="shippingPay" name="itemcount_{{$total_carts}}" value="{{round($shippingQty)}}" />
							<input type="hidden" class="shippingPay" name="itemunitamount_{{$total_carts}}" value="{{number_format($shippingTotal, 2, ',','')}}" /> 
							<input type="hidden" class="shippingPay" name="itemamount_{{$total_carts}}" value="{{number_format($shippingTotal, 2, ',','')}}" /> 
						</div>
						<input type="hidden" name="returnurlsuccess" value="{{$returnSuccess}}" />
						<input type="hidden" name="returnurlsuccessserver" value="{{$returnSuccessServer}}" />
						<input type="hidden" name="returnurlcancel" value="{{$returnCancel}}" />
						<input type="hidden" name="returnurlerror" value="{{$returnError}}" />
						<input type="submit" class="btn btn-primary" value="Greiða" />
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/order.js"></script>
</body>