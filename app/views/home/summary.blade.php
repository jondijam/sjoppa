<!DOCTYPE html>
<html>
<head>
	@include('home/head')
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Sjoppa.is</a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	@foreach($categories as $category)
	      		<li>
	      			<a href="/{{$category->slug}}">{{$category->name}}</a>
	      		</li>
	      	@endforeach
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li>
	        	<a href="/cart">Karfan mín</a></li>
	        		<li class="dropdown">
	          			<a href="/account/profile" class="dropdown-toggle" data-toggle="dropdown">Mín síða <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="/account/profile">Prófíl</a></li>
		            <li><a href="/account/orders">Mínar pantanir</a></li>
		         </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="page-header" id="banner">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-6">
					<h1>Yfirlit yfir pöntun númer {{$order_id}}</h1>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h3>Takk fyrir að velja okkur!</h3>
				<p>Hér er yfirlit yfir það sem þú varst að kaupa.</p>
				<table class="table">
					<thead>
						<tr>
							<th>
								Vörunúmer
							</th>
							<th>
								Nafn á vöru
							</th>
							<th>
								Einingarverð
							</th>
							<th>
								Viðtökunúmer
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $product)
							@for($i = 0; $i < $product->pivot->product_qty; $i++)
								<tr>
									<td>{{$product->ProductId}}</td>
									<td>{{$product->name}}</td>
									<td>{{(($product->retail_price_tax_excl * ((100 + $product->sale_tax)/100)) * ((100 - $product->discount) / 100) )}}</td>
									<td>
										{{$items[$i]->LabelNumber}}
									</td>
								</tr>
							@endfor
						@endforeach
					</tbody>
				</table>
				
			</div>	
		</div>
	</div>
</body>