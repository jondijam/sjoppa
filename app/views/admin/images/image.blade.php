<div class="imageholder" id="imageholder{{{$image_id}}}">
	<figure>
		<img src="{{{$filePath}}}" alt="{{{$fileName}}}" class="image" data-image-id="{{{$image_id}}}"/>
		<span class="fa fa-times-circle remove-images" data-image-id="{{{$image_id}}}"></span>
		<span class="fa @if($primary_image) fa-star @else fa-star-o @endif primary-image" data-image-id="{{{$image_id}}}" data-primary="{{$primary_image}}"></span>
	</figure>
</div>