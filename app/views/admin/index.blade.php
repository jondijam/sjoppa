@extends('admin.layouts.master')
@section('content')
		<div class="row">
			<div class="col-sm-12">
				<div class="page-header">
				  <h1>Dashboard</h1>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Orders</h4>
							</div>
							<div class="panel-body">
								<h1 class="text-center">1.666 <small>orders</small></h1>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Members</h4>
							</div>
							<div class="panel-body">
								<h1 class="text-center">100 <small>users</small></h1>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Sales</h4>
							</div>
							<div class="panel-body">
								<h1 class="text-center">100 <small>users</small></h1>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Revenues</h4>
							</div>
							<div class="panel-body">
								<h1 class="text-center">100.000 <small>kr.</small></h1>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Visitor status</h4>
							</div>
							<div class="panel-body">
								
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Popular products</h4>
							</div>
							<div class="panel-body">
								
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Latest Orders</h4>
							</div>
							<div class="panel-body">
								<table class="table">
									<thead>
										<tr>
											<th>@lang("Order.Orders")</th>
											<th>@lang("Order.Value")</th>
											<th>@lang("Sjoppa.Actions")</th>
										</tr>
									</thead>
									<tbody>
										@if(isset($getLatestOrders))
											@foreach($getLatestOrders as $order)
												<tr>
													<td>
														
													</td>
													<td>
														
													</td>
													<td>
														
													</td>
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop
