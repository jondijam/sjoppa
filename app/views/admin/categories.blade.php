<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin/head')
  </head>
  <body>
    @include('admin/navigation')
    <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Categories</h1>
            <div class="panel panel-default">
            <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    Name
                  </th>
                  <th>
                    Slug
                  </th>
                  <th>
                    Products
                  </th>
                  <th>
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($categories as $category)
                <tr id="{{$category->category_id}}">
                  <td>
                    {{$category->name}}
                  </td>
                  <td>
                    {{$category->slug}}
                  </td>
                  <td>
                  </td>
                  <td>
                    <a href="/admin/categories/edit/{{$category->category_id}}" class="btn btn-primary">Edit</a>
                    <a href="/admin/categories/delete/{{$category->category_id}}" class="delete btn btn-danger" data-category-id="{{$category->category_id}}">  Delete</a>
                  </td>
                </tr>
                 @include('admin/includes/categories', array('parent' => $category->category_id, 'level'=>''))
                @endforeach
              </tbody>
            </table>
              {{$categories->links()}} 
            </div>
            </div>
            <a href="/admin/categories/edit/0" class="btn btn-primary">Create new</a>
          </div>
        </div> 
    </div>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/categories.js"></script>
  </body>
</html>