<!DOCTYPE html>
<html lang="en">
  	<head>
    @include('admin/head')
 	</head>
  	<body>
  		@include('admin/navigation')  
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1>Products</h1>
					<div class="panel panel-default">
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Image</th>
										<th>Name</th>
										<th>Reference</th>
										<th>Category</th>
										<th>(Excl tax) price</th>
										<th>Final price</th>
										<th>Quantity</th>
										<th>Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($products as $product)
										<tr>
											<td>{{$product->ProductId}}</td>
											<td>
												
											</td>
											<td>
												{{$product->name}}
											</td>
											<td>
												{{$product->reference_code}}
											</td>
											<td>
												@foreach($product->categories()->orderBy('parent', 'desc')->get() as $category)
													{{$category->name}}
												@endforeach
											</td>
											<td>
												{{number_format($product->retail_price_tax_excl,0,',','.')}} kr.	
											</td>
											<td>
												{{ number_format($product->retail_price_tax_excl * ((100 + $product->sale_tax) / 100), 0, ',', '.') }} kr.
											</td>
											<td>
												{{$product->qty}}
											</td>
											<td>
												@if($product->active == 0)
												Disable
												@else
												Enable
												@endif
											</td>
											<td>
												<a href="/admin/products/edit/{{$product->ProductId}}" class="btn btn-primary">Edit</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<a href="/admin/products/edit/0" class="btn btn-primary">Create a new product</a>
				</div>
			</div>
		</div>
	</body>
</html>