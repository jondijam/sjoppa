<!DOCTYPE html>
<html lang="en">
<head>
	 @include('admin/head')
</head>
<body>
	@include('admin/navigation')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit user</h1>
				<form action="/admin/users/save/{{$user_id}}" method="post">
				<div class="panel panel-default">
					<div class="panel-body">
						@if ($errors->has())
						
							@foreach ($errors->all() as $error)
								{{ $error }}		
							@endforeach
						
						@endif
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" name="username" value="{{$username}}"  @if($user_id > 0) readonly="readonly" @endif required />
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" name="email" value="{{$email}}" required />
						</div>
						@if($user_id > 0)
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" name="password" value="" required />
						</div>
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" class="form-control" name="password_confirm" value="" required />
						</div>
						@endif
						<div class="form-group">
							<input type="submit" class="btn btn-primary" name="save" value="Save" />
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>