<!DOCTYPE html>
<html>
<head>
	@include('admin.includes.head')
</head>
<body>
	@include('admin.includes.menu')
	<div class="container">
		@yield('content')
	</div>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
  	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
  	@yield('javascript')
</body>
</html>