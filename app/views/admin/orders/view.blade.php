<!DOCTYPE html>
<html lang="en">
  	<head>
    	@include('admin/head')
  	</head>
	<body>
		@include('admin/navigation')
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2>Order {{$orderId}}</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8 col-md-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Payment Status</h3>
						</div>
						<div class="panel-body">
							{{$paymentStatus}}
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Status</h3>
						</div>
						<div class="panel-body">
							<select name="status" class="form-control">
								<option @if($status == "in progress") selected="selected" @endif value="in progress">In progress</option>
								<option @if($status == "shipped") selected="selected" @endif value="shipped">Shipped</option>
								<option @if($status == "delivered") selected="selected" @endif value="delivered">Delivered</option>
							</select>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Shipping</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Carrier</th>
										<th>Weight</th>
										<th>Shipping cost</th>
										<th>Tracking number</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											{{$shippingDate}}
										</td>
										<td>{{$carrier}}</td>
										<td>{{number_format($weight, 2)}} kg</td>
										<td>{{$totalShipping}} ISK</td>
										<td>{{$trackingNumber}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Payment</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Payment method</th>
										<th>Transaction ID</th>
										<th>Amount</th>
										<th>Invoice Id</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											{{$createdAt}}
										</td>
										<td>
											{{$paymentMethod}}
										</td>
										<td>
											{{$transactionId}}
										</td>
										<td>
											{{$amount}}
										</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Products</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Qty</th>
										<th>Price</th>
										<th>Label number</th>
									</tr>
								</thead>
								<tbody>
									@foreach($products as $product)
										<tr>	
											<td>
												{{$product->name}}
											</td>
											<td>
												{{$product->pivot->product_qty}}
											</td>
											<td>
												{{round($product->pivot->total_price_tax_incl)}}
											</td>
											<td>
											{{$labels[$product->ProductId]}}

											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Customer</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Address</th>
										<th>Country</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{$customerName}}</td>
										<td>{{$customerAddress}} {{$customerZip}} {{$customerCity}}</td>
										<td>{{$customerCountry}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Shipping Address</h3>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Address</th>
										<th>Country</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{$shippingName}}</td>
										<td>{{$shippingAddress}} {{$shippingZip}} {{$shippingCity}}</td>
										<td>{{$shippingCountry}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
