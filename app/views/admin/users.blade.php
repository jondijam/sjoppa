<!DOCTYPE html>
<html lang="en">
	<head>
		 @include('admin/head')
	</head>
	<body>
		@include('admin/navigation') 
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1>Users</h1>
					<div class="panel panel-default">
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Username</th>
										<th>Email</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr>
										<td>
											{{$user->username}}
										</td>
										<td>
											{{$user->email}}
										</td>
										<td>
											<a href="/admin/users/edit/{{$user->user_id}}">Edit</a>
											<a href="/admin/users/delete/{{$user->user_id}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>

						</div>
					</div>
					<a href="/admin/users/edit/0" class="btn btn-primary">Create a user</a>
				</div>
			</div>
		</div> 
	</body>
</html>