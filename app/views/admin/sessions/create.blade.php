@extends('admin.layouts.master')
@section('content')
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Admin - login</h4>
				</div>
				<div class="panel-body">
				{{ Form::open(['route' => 'admin.sessions.store']) }}
					<div class="form-group">
						{{ Form::label('username', Lang::get('Sjoppa.Username')) }}
						{{ Form::text('username', $value = null, $attributes = array('class'=>'form-control', 'placeholder'=> Lang::get('Sjoppa.Username'))) }}
						<label class="label label-danger">{{ $errors->loginForm->first('username')}}</label>
					</div>
					<div class="form-group">
						{{ Form::label('password', Lang::get('Sjoppa.Password')) }}
						{{ Form::password('password', $attributes = ['class' => 'form-control', 'placeholder'=> Lang::get('Sjoppa.Password')]) }}
						<label class="label label-danger @if(!$errors->loginForm->has('password') or $error == "" ) hidden @endif">
							@if($errors->loginForm->has('password'))
							{{ $errors->loginForm->first('password')}}
							@elseif($error != "")
							{{ $error }}
							@endif
						</label>
					</div>
					<div class="form-group">
						{{ Form::submit(Lang::get('Sjoppa.Login'), $attributes = ['class' => 'btn btn-primary'] ) }}
					</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop