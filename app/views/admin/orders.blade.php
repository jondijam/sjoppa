<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin/head')
  </head>
  <body>
    @include('admin/navigation')
    <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Orders</h1>
            <div class="panel panel-default">
              <div class="panel-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Status</th>
                      <th>Ship to</th>
                      <th>Date</th>
                      <th>Total</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($orders as $order)
                       <tr id="{{$order->order_id}}">
                      <td>{{$order->order_id}}</td>
                      <td>{{$order->status}}</td>
                      <td>
                        @if($order->customers()->where('is_shipping', 1)->count() > 0)
                          {{$order->customers()->where('is_shipping', 1)->first()->name }}, 
                          {{$order->customers()->where('is_shipping', 1)->first()->address }} 
                          {{$order->customers()->where('is_shipping', 1)->first()->zip }} 
                          {{$order->customers()->where('is_shipping', 1)->first()->city }}
                        @endif
                      </td>
                      <td>{{$order->created_at}}</td>
                      <td>{{round($order->total_paid)}} kr.</td>
                      <td>

                        <a href="/admin/order/view/{{$order->order_id}}" class="btn btn-primary">View</a> 
                        <a href="/admin/order/delete/{{$order->order_id}}" data-order-id="{{$order->order_id}}" class="delete btn btn-danger">Delete</a></td>
        
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                 {{$orders->links()}}
              </div>
            </div>
          </div>
        </div> 
    </div>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/order.js"></script>
  </body>
</html>