@extends('admin.layouts.master')
@section('content')
	<div class="row">
		<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Status</th>
									<th>Orders</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $product)
									<tr>
											<td>{{$product->name}}</td>
											<td>{{ Lang::choice('products.Stocks', 10, array('qty' => 10)) }}</td>
											<td>{{ Lang::choice('products.Orders', count($product->orders), array('qty' => count($product->orders))) }}</td>
											<td>
												{{
													link_to_route('admin.products.edit', Lang::get("sjoppa.Edit"), $parameters = array("products" => $product->id), $attributes = array("class"=>"btn btn-primary"));
												}}
												{{
													link_to_route('admin.products.destroy', Lang::get("sjoppa.Delete"), $parameters = array("products" => $product->id), $attributes = array("class"=>"btn btn-danger"));
												}}
											</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				{{
				link_to_route('admin.products.create', Lang::get("sjoppa.Create"), $parameters = array(), $attributes = array("class"=>"btn btn-primary"));
				}}
			</div>
	</div>
@stop