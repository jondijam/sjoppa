<?php 
	$categories = Category::where('parent', $parent)->get(); 
?>

<ul>
@foreach($categories as $category)
	<?php 

	$productCategory = $category->find($category->category_id); 
	$product = $productCategory->products()->where('ProductId', $product_id)->first()
	?>
	<li>
		<label class="checkbox">
			<input type="checkbox" name="categories[]" @if(count($product) > 0) checked="checked" @endif value="{{$category->category_id}}" />  
			{{$category->name}}

		</label>
		 @include('admin/products/categories', array('parent' => $category->category_id, 'level'=>0, 'product_id'=>$product_id))
	</li>

@endforeach
</ul>
