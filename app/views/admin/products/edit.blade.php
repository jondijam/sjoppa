@extends('admin.layouts.master')
@section('content')
  <div class="row">
    {{  Form::model($product, array('route' => array('admin.products.update', $product->id), 'method' => 'put', 'date-remote')) }}
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4>Images</h4>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-6">
              <h3>Gallery</h3>
            </div>
            <div class="col-sm-6">
              <h3>Upload</h3>
              <div id="droparea" data-product-id="{{$product->id}}">
                 <div class="dropareainner">
                    <p class="dropfiletext">Drop files here</p>
                    <p>or</p>
                    <p>
                    <input id="uploadbtn" type="button" value="Select files"/></p>
                    <!-- extra feature -->
                    <p id="err"><!-- error message -->
                    </p>
                 </div>
                 <input id="upload" type="file" multiple="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{ Form::close()}}
  </div>
@stop
@section("javascript")
  <script type="text/javascript" src="/js/products.js"></script>
  <script src="/js/modernizr.custom.js"></script>
  <script src="/js/script.js"></script>
@stop