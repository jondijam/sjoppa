<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin/head')
  </head>
  <body>
    @include('admin/navigation')
    <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Categories</h1>
            @if ($errors->has())
            <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
              {{ $error }}    
            @endforeach
            </div>
            @endif
            <div class="panel panel-default">
            <div class="panel-body">
              <form action="/admin/categories/save/{{$category_id}}" method="post">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" class="form-control" value="{{$name}}" />
                </div>  
                <div class="form-group">
                  <label>Parent</label>
                  <select class="form-control" name="parent">
                    <option value="0">No Parent</option>
                    @foreach($categories as $category)
                        <option @if($category->category_id == $parent) selected="selected" @endif value="{{ $category->category_id}}">{{$category->name}}</option>
                        @include('admin/includes/categories-select', array('category_id'=>$category_id, 'parent' => $category->category_id, 'level'=>''))
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <input type="submit" value="Save" class="btn btn-primary">
                </div>

              </form>
            </div>
            </div>
          </div>
        </div> 
    </div>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/categories.js"></script>
  </body>
</html>