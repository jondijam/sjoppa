<?php 
	$categories = Category::where('parent', $parent)->get(); 
?>

@foreach ($categories as $category)
                <tr id="{{$category->category_id}}">
                  <td>
                    - {{$level}}{{$category->name}}
                  </td>
                  <td>
                    {{$category->slug}}
                  </td>
                  <td>
                  </td>
                  <td>
                    <a href="/admin/categories/edit/{{$category->category_id}}" class="btn btn-primary">Edit</a>
                    <a href="/admin/categories/delete/{{$category->category_id}}" class="delete btn btn-danger" data-category-id="{{$category->category_id}}">  Delete</a>
                  </td>
                </tr>
                @include('admin/includes/categories', array('parent' => $category->category_id, 'level'=>'- '))

@endforeach