<?php $categories = Category::where('category_id', '!=', $category_id)->where('parent', $parent)->get(); ?>

@foreach($categories as $category)
    <option @if($category->category_id == $parent) selected="selected" @endif value="{{ $category->category_id}}">- {{$level}}{{$category->name}}</option>
    @include('admin/includes/categories-select', array('parent' => $category->category_id, 'level'=>'- '))
@endforeach