 <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li @if($segment == "") class="active" @endif><a href="/admin">Home</a></li>
          <li @if($segment == "orders") class="active" @endif><a href="/admin/orders">Orders</a></li>
          <li @if($segment == "categories") class="active" @endif><a href="/admin/categories">Categories</a></li>
          <li @if($segment == "products") class="active" @endif><a href="/admin/products">Products</a></li>
          <li @if($segment == "users") class="active" @endif><a href="/admin/users">Users</a></li>
          {{--
          <li @if($segment == "attributes") class="active" @endif><a href="/admin/attributes">Attributes</a></li>
          <li @if($segment == "manufacturers") class="active" @endif><a href="/admin/manufacturers">Manufacturers</a></li>
          --}}
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/admin/logout">Logout</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>