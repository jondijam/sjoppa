<?php namespace Sjoppa\Repo;


abstract class DbRepo 
{
	protected $model;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function getById($id)
	{
		return $this->model->find($id);
	}

	public function save($data, $id)
	{
		if($id > 0) 
		{
			$model = $this->getById($id);
			$model = $model->fill($data)->save();

			return $model;
		}

		$model = $this->model->create($data);
		return $model;
	}

	public function delete($id)
	{
		$model = $this->getById($id);
		$model->delete();
	}

	public function getAll()
	{
		return $this->model->all();
	}
}

?>