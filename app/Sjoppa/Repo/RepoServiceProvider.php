<?php namespace Sjoppa\Repo;
use Illuminate\Support\ServiceProvider;

use Product;
use Category;
use User;
use Order;

use Sjoppa\Service\Cache\LaravelCache;
use Sjoppa\Repo\Product\CacheDecorator;
use Sjoppa\Repo\Product\EloquentProduct;
use Sjoppa\Repo\Category\EloquentCategory;
use Sjoppa\Repo\User\EloquentUser;
use Sjoppa\Repo\Order\EloquentOrder;


class RepoServiceProvider extends ServiceProvider
{
	/**
     * Register the service provider.
     *
     * @return void
     */

	public function register()
	{
		$app = $this->app;
		
		$app->bind('Sjoppa\Repo\Product\ProductInterface', function($app)
		{
			$product = new EloquentProduct(new Product);
			return new CacheDecorator($product, new LaravelCache($app['cache'], 'products', 10));
		});

		$app->bind('Sjoppa\Repo\Category\CategoryInterface', function($app)
		{
			return new EloquentCategory(new Category);
		});


		$app->bind('Sjoppa\Repo\User\UserInterface', function($app)
		{
			return new EloquentUser(new User);
		});

		$app->bind('Sjoppa\Repo\Order\OrderInterface', function($app)
		{
			return new EloquentOrder(new Order);
		});
	}
}

?>