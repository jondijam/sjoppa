<?php namespace Sjoppa\Repo\User;

	interface UserInterface
	{
		public function getById($id);
		public function getAll();
	}
	
?>