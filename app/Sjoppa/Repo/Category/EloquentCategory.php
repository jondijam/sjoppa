<?php namespace Sjoppa\Repo\Category;

use Sjoppa\Repo\DbRepo;
use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class EloquentCategory extends DbRepo implements CategoryInterface
{
	protected $model;
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
}

?>