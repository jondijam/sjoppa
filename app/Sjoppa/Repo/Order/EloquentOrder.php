<?php namespace Sjoppa\Repo\Order; 

	use Illuminate\Database\Eloquent\Model;
	use Sjoppa\Repo\DbRepo;

	class EloquentOrder extends DbRepo implements OrderInterface
	{
		protected $model;

		function __construct(Model $model)
		{
			$this->model = $model;
		}

		public function getLatest($limit = 10)
		{
			return $this->model->orderBy("created_at")->take($limit)->get();

		}
	}

?>