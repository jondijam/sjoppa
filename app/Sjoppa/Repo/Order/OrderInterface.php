<?php namespace Sjoppa\Repo\Order;  

interface OrderInterface
{
	public function getById($id);
	public function getAll();
	public function getLatest($limit = 10);
}

?>