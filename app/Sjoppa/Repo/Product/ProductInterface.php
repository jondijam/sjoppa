<?php namespace Sjoppa\Repo\Product;

	interface ProductInterface
	{
		public function getById($id);
		public function getAll();
		public function getByPage($page, $limit, $all = false);
		public function getByOrders();
	}



?>