<?php namespace Sjoppa\Repo\Product;

use Illuminate\Database\Eloquent\Model;
use Sjoppa\Repo\DbRepo;

/**
* 
*/
class EloquentProduct extends DbRepo implements ProductInterface
{
	protected $model;
	
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function getByPage($page, $limit, $all = false)
	{
		$result = new \StdClass;
		
		$result->page = $page;
		$result->limit = $limit;
		$result->totalItems = 0;
		$result->items = array();

		$products = $this->model
		->with('orders')
		->orderBy('created_at', 'desc')
		->skip( $limit * ($page - 1) )
		->take($limit)
		->get();

		$result->items = $products->all();
 		$result->totalItems = $this->totalProducts();

 		return $result;
	}

	public function getByOrders()
	{
		return $this->model->with('Orders')->get();
	}

	protected function totalProducts()
	{
		return $this->model->count();
	}
}
?>