<?php namespace Sjoppa\Repo\Product;

abstract class AbstractProductDecorator implements ProductInterface
{
	protected $nextProduct;

	public function __construct(ProductInterface $nextProduct)
	{
		$this->nextProduct = $nextProduct;
	}

	public function getById($id)
	{
		return $this->nextProduct->getById($id);
	}

	public function getAll()
	{
		return $this->nextProduct->getAll();
	}
	
	public function getByPage($page, $limit = 10, $all = false)
	{
		return $this->nextProduct->getByPage($page, $limit, $all);
	}

	public function getByOrders()
	{
		return $this->nextProduct->getByOrders();
	}
}


?>