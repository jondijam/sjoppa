<?php namespace Sjoppa\Repo\Product;

use Sjoppa\Service\Cache\CacheInterface;

class CacheDecorator extends AbstractProductDecorator
{
	protected $cache;

	public function __construct(ProductInterface $nextProduct, CacheInterface $cache)
	{
		parent::__construct($nextProduct);
		$this->cache = $cache;			
	}

	public function getById($id)
	{
		$key = md5("id.".$id);

		if( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$product = $this->nextProduct->getById($id);
		$this->cache->put($key, $product);

		return $product;
	}

	public function getByPage($page, $limit = 10, $all = false) 
	{
		
	}
}

?>