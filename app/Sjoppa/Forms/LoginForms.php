<?php namespace Sjoppa\Forms;

use Laracasts\Validation\FormValidator;

class LoginForms extends FormValidator 
{
	protected $rules = [
		'username'    => 'required',
		'password' => 'required'
	];
} 