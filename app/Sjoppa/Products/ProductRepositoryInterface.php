<?php  namespace Sjoppa\Products;

interface ProductRepositoryInterface
{
	public function getAll();
	public function getByPage($page = 1, $limit = 10);
	public function find($id);
}


?>