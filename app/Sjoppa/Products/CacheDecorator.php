<?php namespace Sjoppa\Products;

use Sjoppa\Service\Cache\CacheInterface;

/**
* 
*/
class CacheDecorator extends AbstractUserDecorator
{
	protected $cache;

	function __construct(ProductRepositoryInterface $product, CacheInterface $cache)
	{
		parent::__construct($product);
		$this->cache = $cache;	
	}

	 /**
   * All
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function get()
  {
    $key = md5('all');
 
    if($this->cache->has($key))
    {
      return $this->cache->get($key);
    }
 
    $users = $this->user->all();
 
    $this->cache->put($key, $users);
 
    return $users;
  }
}


?>