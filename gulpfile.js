var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var sass = require('/usr/lib/node_modules/gulp-ruby-sass');
var autoprefix = require('/usr/lib/node_modules/gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var bower = require('/usr/lib/node_modules/gulp-bower');
var coffee = require('gulp-coffee');
var exec = require('child_process').exec;
var sys = require('sys');



var paths = {
    'dev': {
        'coffeeDir':'app/assets/coffee',
        'targetJSDir':'public/js',
        'sassDir':'app/assets/sass',
        'bowerDir':'public/components',
        'targetCSSDir':'public/css/min'
    }
}

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(paths.dev.bowerDir)) 
});


gulp.task('icons', function() { 
    return gulp.src(paths.dev.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('public/fonts')); 
});

gulp.task('sass', function () {
    
    return gulp.src(paths.dev.sassDir+'/admin.scss')
    .pipe(sass({
        style: 'compressed',
        loadPath: [
                paths.dev.sassDir,
                 paths.dev.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                 paths.dev.bowerDir + '/fontawesome/scss'
         ]
    })).on('error', gutil.log)
    .pipe(gulp.dest(paths.dev.targetCSSDir));
});

gulp.task('css', function()
{
    return gulp.src(paths.dev.targetCSSDir+'/admin.css')
    .pipe(autoprefix('last 10 versions'))
    .pipe(gulp.dest(paths.dev.targetJSDir)); 
});

gulp.task('js', function () {
    return gulp.src(paths.dev.coffeeDir + '/**/*.coffee')
        .pipe(coffee().on('error', gutil.log))
        .pipe(gulp.dest(paths.dev.targetJSDir))
});
 
// Run all PHPUnit tests
gulp.task('phpunit', function() {
    exec('phpunit', function(error, stdout) {
        sys.puts(stdout);
    });
});

// Keep an eye on Sass, Coffee, and PHP files for changes...
gulp.task('watch', function () {
    gulp.watch(paths.dev.sassDir + '/**/*.scss', ['sass']);
    gulp.watch(paths.dev.coffeeDir + '/**/*.coffee', ['js']);
    gulp.watch('app/**/*.php', ['phpunit']);
});
 
// What tasks does running gulp trigger?
gulp.task('default', ['bower','icons', 'sass', 'css', 'js', 'phpunit', 'watch']);

    
